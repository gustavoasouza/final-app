const express       = require('express')
const handlebars    = require('express-handlebars')
const bodyParser    = require('body-parser')
const app           = express()
const admin         = require('./routes/admin')
const mongoose      = require('mongoose')
const session       = require('express-session')
const flash         = require('connect-flash')
                      require('./models/Postagem')
const Postagem      = mongoose.model('postagens')
                      require('./models/Categoria')
const Categoria     = mongoose.model('categorias')
const usuarios      = require('./routes/usuario')
const passport      = require('passport')
                      require('./config/auth')(passport)

//Usado para trabalhar com diretórios
const path = require('path')

//Configurações
    //Sessão
    app.use(session({
        secret: 'cursodenode',
        resave: true,
        saveUninitialized: true
    }))

    //Passport
    app.use(passport.initialize())
    app.use(passport.session())

    //Flash
    app.use(flash())

    //Middleware
    app.use((req, res, next) => {
        //Criando variaveis globais
        res.locals.success_msg  = req.flash('success_msg')
        res.locals.error_msg    = req.flash('error_msg')
        res.locals.error        = req.flash('error')
        res.locals.user         = req.user || null
        next()
    })

    //Body Parser
    app.use(bodyParser.urlencoded({extended: false}))
    app.use(bodyParser.json())

    //Handlebars
    app.engine('handlebars', handlebars({defaultLayout: 'main'}))
    app.set('view engine', 'handlebars')

    //Mongoose
    mongoose.Promise = global.Promise
    mongoose.connect('mongodb://localhost/finalapp', {
        useNewUrlParser: true
    }).then( () => {
        console.log('Concetado ao mongodb')
    }).catch( (erro) => {
        console.log(`Erro ao se conectar: ${erro}`)
    })

    //Public
    //Definindo pasta de arquivos estaticos
    app.use(express.static(path.join(__dirname, 'public')))

//Rotas
//Criando uma rota sem prefixo
app.get('/', (req, res) => {
    //Retornando postagens do modulo Postagem
    Postagem.find().populate('categoria').sort({data: 'desc'}).then( (postagens) => {
        res.render('index', {postagens: postagens})
    }).catch( (erro) => {
        req.flash('error_msg', 'Houve um erro interno')
        res.redirect('/404')
    })
})

app.get('/postagem/:slug', (req, res) => {
    //Pesquisando uma postagem pelo slug dela
    Postagem.findOne({slug: req.params.slug}).then( (postagem) => {

        if (postagem) {
            res.render('postagem/index', {postagem: postagem})
        } else {
            req.flash('error_msg', 'Esta postagem não existe')
            res.redirect('/')
        }

    }).catch( (erro) => {
        req.flash('error_msg', 'Houve um erro interno')
        res.redirect('/')
    })
})

app.get('/404', (req, res) => {
    res.send('Erro 404!')
})

app.get('/categorias', (req, res) => {
    //Find para listar todas    
    Categoria.find().then( (categorias) => {
        res.render('categorias/index', {categorias: categorias})
    }).catch( (erro) => {
        req.flash('error_msg', 'Houve um erro ao listar as categorias')
    })
})

app.get('/categorias/:slug', (req, res) => {
    Categoria.findOne({slug: req.params.slug}).then( (categoria) => {

        if (categoria) {

            //Pesquisar os posts que pertemcem as categorias passadas pelo slug
            Postagem.find({categoria: categoria._id}).then( (postagens) => {

                res.render('categorias/postagens', {postagens: postagens, categoria: categoria})

            }).catch( (erro) => {
                req.flash('error_msg', 'Houve um erro ao listar os posts')
            })


        } else {
            req.flash('error_msg', 'Esta categoria não existe')
            res.redirect('/')
        }

    }).catch( (erro) => {
        req.flash('error_msg', 'Houve um erro interno ao carregar a página desta categoria')
        res.redirect('/')
    })
})

//Acessando o grupo de rotas admin, passando o prefixo /admin e /usuario
app.use('/admin', admin)
app.use('/usuarios', usuarios)

//Outros
const PORT = 3000
app.listen(PORT, () => {
    console.log(`Servidor rodando na porta: ${PORT}`)
})