# Blog

## Functionalities
- Add posts
- Add categories
- Edit posts and categories
- Login system
- Encrypt passwords

## Frameworks
- Handlebars.js
- Express.js
- Mongoose
- Passport.js
- Bcrypt