const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Postagem = new Schema({
    titulo: {
        type: String,
        require: true
    },

    slug: {
        type: String,
        required: true
    },

    descricao: {
        type: String,
        require: true
    },

    conteudo: {
        type: String,
        required: true
    },

    //Referenciar uma categoria que já existe
    //Criando um relacionamento entre o model Categoria e Postagem
    categoria: {
     type: Schema.Types.ObjectId,
     //ref é o nome do model de referência de onde irá pegar o id
     ref: 'categorias',
     required: true
    },

    data: {
        type: Date,
        //Definindo valor padrão da data
        default: Date.now()
    }
})

mongoose.model('postagens', Postagem)